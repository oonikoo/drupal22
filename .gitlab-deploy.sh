#!/bin/bash
#Get servers list
set -f
string=$aws
array=(${string//,/ })
#Iterate servers for deploy and pull last commit
for i in "${!array[@]}";do
      ssh ubuntu@${array[i]} "cd /home/ubuntu/my_drupal/ && git pull && pip install -r requirements.txt && python3 test.py"
done
