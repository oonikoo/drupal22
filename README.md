# Proyecto 2 tópicos especiales de telemática

# Tabla de Contenidos

- [Desarrolladores](#desarrolladores)
- [Asignación de roles y responsabilidades de cada integrante del equipo en el desarrollo del Proyecto 2](#asignaci-n-de-roles-y-responsabilidades-de-cada-integrante-del-equipo-en-el-desarrollo-del-proyecto-2)
- [Aplicación Seleccionada](#aplicaci-n-seleccionada)
- [Información de la aplicación](#informaci-n-de-la-aplicaci-n)
- [Especificación de requisitos no funcionales](#especificaci-n-de-requisitos-no-funcionales)
    + [Disponibilidad](#disponibilidad)
    + [Rendimiento](#rendimiento)
    + [Seguridad](#seguridad)
- [Instalación](#instalaci-n)
- [DCA](#dca)
- [Amazon Web Services](#amazon-web-services)
  * [Instalación de Drupal, balanceador de cargas Nginx e integración continua con GitLab](#instalaci-n-de-drupal--balanceador-de-cargas-nginx-e-integraci-n-continua-con-gitlab)
  * [Configuración del análisis de código estático](#configuraci-n-del-an-lisis-de-c-digo-est-tico)
  * [Configuración CDN](#configuraci-n-cdn)
  * [Configuración de dominio público](#configuraci-n-de-dominio-p-blico)
  * [Instalación y configuración de certificados SSL](#instalaci-n-y-configuraci-n-de-certificados-ssl)
  * [Configuración de login con cuenta Google](#configuraci-n-de-login-con-cuenta-google)
  * [Configuración Amazon RDS](#configuraci-n-amazon-rds)



# Desarrolladores

- Nicolás Muñoz Estrada - nmunoze@eafit.edu.co - Universidad EAFIT
- Joshua Sánchez Álvarez - jsanch90@eafit.edu.co - Universidad EAFIT
- Camilo Villa Restrepo - cvillar4@eafit.edu.co - Universidad EAFIT

# Asignación de roles y responsabilidades de cada integrante del equipo en el desarrollo del Proyecto 2
- DevOps - Nicolás Muñoz Estrada
- Seguridad - Joshua Sánchez Álvarez
- AWS - Camilo Villa Restrepo
# Aplicación Seleccionada

<div align="center"> 
<img src="https://www.comerline.es/wp-content/uploads/2018/05/drivemeca-drupal-logo.png">
</div>

Para el desarrollo de este proyecto seleccionamos el sistema de gestión de aprendizaje (LMS) 
[Drupal](https://www.drupal.org/).
 Inicialmente la instalamos y desplegamos en el DCA de la universidad como ambiente de desarrollo y pruebas y posteriormente se utilizo Amazon Web Services para desplegar la aplicación en un entorno de producción.
# Información de la aplicación
- DCA:
	- Dirección IP: [192.168.10.212](http://192.168.10.212)
	- Dominio en el DCA: [proyecto22.dis.eafit.edu.co](http://proyecto22.dis.eafit.edu.co/)
	- Sistema Operativo: CentOS Linux 7
	- Motor de base de datos: Postgres
	- Version de Drupal: Drupal 8
	- Version de Docker: 19.03.6
	- Version de Docker Compose: 1.25.3
- AWS:
	- Dirección IP: [34.195.129.154](http://34.195.129.154)
	- Dominio: [https://proyecto22.tk](https://proyecto22.tk)
	- Sistema Operativo: Ubuntu 18.04.3 LTS
	- Motor de base de datos: Postgres
	- Version de Drupal: Drupal 8
	- Version de Docker: 19.03.6
	- Version de Docker Compose: 1.25.3
	- Balanceador de cargas: [Nginx](https://www.nginx.com/)
- Repositorio:
	- GitLab: [https://gitlab.com/oonikoo/drupal22.git](https://gitlab.com/oonikoo/drupal22.git)

# Especificación de requisitos no funcionales
## Disponibilidad
Para asegurar este requisito hicimos uso de distintas herramientas y servicios, entre ellos [Amazon RDS](https://docs.aws.amazon.com/es_es/AmazonRDS/latest/UserGuide/Welcome.html) el cual nos ofrece una gran variedad de beneficios como autoescalamiento de la base de datos sobre la demanda, identificación y recuperación de errores, copias de seguridad automáticas y alta disponibilidad con una instancia principal y una instancia secundaria síncrona. Con todo esto podemos garantizar que la aplicación va a tener un alto grado de disponibilidad brindando una buena experiencia al usuario.
También utilizamos un balanceador de cargas, esto es útil para este requisito debido a que, si una de las máquinas en las que corre la aplicación presenta errores y se cae, hay mas maquinas respaldando esto y las peticiones van a ser redireccionadas a las máquinas que están arriba por el balanceador de cargas sin afectar la disponibilidad de la aplicación.
## Rendimiento 
Este requisito lo aseguramos implementando un balanceador de cargas, un servicio de CDN, y el servicio de Amazon RDS también nos apoya en ese sentido. El balanceador de carga ayuda a descongestionar las maquinas que corren la aplicación distribuyendo las peticiones entre todas ellas, así evitamos que todas las peticiones vayan a una sola máquina afectando los tiempos de respuesta. El servicio de *Content Delivery Network* o CDN utilizamos la plataforma [KeyCDN](https://www.keycdn.com/) la cual nos permite configurar zonas CDN de manera sencilla y con esto mejoramos el rendimiento de la aplicación ya que redistribuyen localmente el contenido de los servidores y guardan en caché los archivos que no necesitan actualización permanente evitando ir a consultar directamente al servidor donde se encuentran estos archivos. El servicio de RDS nos beneficia debido que las instancias de las bases de datos crecen sobre la demanda evitando que se congestione y se caiga la conexión con la base de datos.
### Seguridad
Para la seguridad tomamos varias medidas, instalamos certificados de seguridad SSL para asegurar que las peticiones en la aplicación estuviesen cifradas, además implementamos un login utilizando cuentas de Google para que el usuario se sienta seguro creando una cuenta en nuestra aplicación. Por otro lado revisamos las políticas de las contraseñas de Drupal para asegurarnos que las contraseñas se están creando y almacenando de una manera óptima y segura.

# Instalación
# DCA
1. Activar el VPN y concentrarse con la dirección IP asignada:**![](https://lh3.googleusercontent.com/SwXbj4yyRZFnbdtCpa77smddyIAgllAMWlrTeWYGZk4jC0e8cCZLyQKOuAEY6uxge6BrYFgY9kO-BOh-TGX2OoVxUG6SwZkkS9X5RdORhJERcYBjUuq5W6NN72aHjy-4gLAtexDv)**
2. Instalar git: ```$ yum install git```
3.  Instalar Docker:
	- Instalar dependencias necesarias:   
	```$ sudo yum install -y yum-utils device-mapper-persistent-data lvm2```
	- Añadir el repositorio de docker estable:  
    ```$ sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo```
	- Instalar la docker:  
    ```$ sudo yum install docker-ce docker-ce-cli containerd.io```
    - Iniciar docker:  
    ```$ sudo systemctl start docker```
4. Instalar Docker Compose:
	- Descargar docker compose:	
	```$  sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose```
	- Dar permisos de ejecución al binario: 
	```$ sudo chmod +x /usr/local/bin/docker-compose```
	- Crear un enlace simbólico en el directorio bin del sistema para que sea ejecutable desde cualquier directorio: 
	```$ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose```

5. Descargar el repositorio con las configuraciones de docker de y nginx para drupal:
	```$ git clone https://gitlab.com/oonikoo/drupal22.git```
![](https://lh4.googleusercontent.com/9dyUEJ-ZF-WS10QmmlPmZ6dcogz9-K6GZ97mmMZ4XCBbjCFVDZWWs2RLBH9cZZviEpQlNZwcJPnf7x4ys4q81DsGdqjZ3HRK6721FzQgyMfjkfqwv3ER23l5colGpB1GxGP3rcnB)
6. Entrar al directorio “drupal22” y ejecutar el siguiente comando: 
	```$ sudo docker-compose up```
	Se descargan las respectivas imágenes de docker, se crearán los contenedores y se ejecutarán
7. Para verificar que los contenedores iniciaron correctamente ejecutamos el siguiente comando: ![](https://lh5.googleusercontent.com/PWff9z8358u9F1YiMako55R_Hnzm0NHF2Dbx5bkK_jd0gMMR90RgkK1yBT7Na2G4jnxsHjkLbZbvjFrtEEc76s4-KrUmjbpsgN0krXXlAxopTC4rjZj1M8Doam5NFrn2CScH9B0G) Aquí se puede ver toda la información de los contenedores como su ID, la imagen con que se creó el contenedor, hace cuanto fue instanciado el contenedor, es estado del contenedor, el mapeo de los puertos que se están usando, y los nombres de los contenedores.
8. En este caso nuestra aplicación está corriendo en el puerto 80, si visitamos la ip de la máquina en este puerto debemos ver la aplicación. ![](https://lh5.googleusercontent.com/KxZ9Q3k9d9XXyxedByskm_FTtv-qxTTQ2bUIZCZJoWGt8ZUlOb7iM-zVCM06keMz2oQtTxt2w_H52prGshVi7iYNmKCuLhB8XBScfeNg4yjyPzVFMsEBsLDlKaPqp9Vf4oEpVaDu)

# Amazon Web Services
## Instalación de Drupal, balanceador de cargas Nginx e integración continua con GitLab
1. Seleccionamos la versión 18 de ubuntu y configuramos el security group para poder exponer servicios en el puerto 80 (http) y también permitir la conexión ssh mediante el puerto 22.
**![](https://lh6.googleusercontent.com/SaRvJ2QVmEB1iJ0UjeHC1M0QC8sXUCiR8k_8tUM_cfIvrAoEovyV3cQDcnYBt8rw9BWLk4OHxbm6lWhvbkNFzo9zOtA5o8ZCehlBxYoQO69VMTdIEL78EK0tlAHg3FPzwGMaCeY)**
**![](https://lh5.googleusercontent.com/RsFRQU0_bIBGLAo1l36PKbvBQR0yg77wY-YOmfrvQvL-jpc5B-GcMKEEmW19ZCTNX1R_Z5vZ-R5vWeJGtQtgxjK6ZNc9ctFQmQZmEutMdyr9jVvW4j0cMgToM-xMvlLPQh9aQkY)**
**![](https://lh5.googleusercontent.com/3JXBMIAXVKhKhgVuQy0MC7vZ8ZRlmqgdpJwXy0PwuRPn-Atmg5enB6i64h2TehxMjiOdSHn0Xmq18VQe9GwnVfWGUD7HeEJfSpE4RKeC1FnDoVfyrXZs0GDKxN7Cp02LYR714D8)**
2. Configurando security group:**![](https://lh6.googleusercontent.com/YJ_-XeO4yHkSoXSCP44PPjnCx5v8ajdpEXsr4CYwGYxSda0fGChvyuibpzuk2JNNa1ocsHEbZKmfVgIp7-NRPuObiA1hLfo5cdOVQFo2B66UyT1zU24k5a5iBOnt80fHxSbY22Y)**
3. Instalamos Drupal mediante docker y docker-compose: **![](https://lh6.googleusercontent.com/pPrGETEvbqGPucI5genwE0DGEpkQvi4ZltfFR841Qdiey5B-4qCUonQ51rhZ77NJNonW183dhzOmOTHuHx5lU435vBWiZCnREhV8Le89YhjcII3cBBkLH5msSWG5-TWb4adv-lg)**
4. Integración continua con gitlab: Configuramos las variables de ambiente necesarias en Gitlab y definimos los jobs en el archivo ```gitlab-deploy.sh```**![](https://lh3.googleusercontent.com/B4B-yLdYTxqkVsU5zaQjcU5HeLQTxdXcn40xLkonCNbwkPEtKdhpUHixYYWA3CJGYG2ovJj9So0mLVf5MBucQ5OGC3XaO8e3i8IjIqZCTQi0b-s3zt9t4p2WRshGvCKQEMCiUxY)**
**![](https://lh5.googleusercontent.com/e8L12fTQNoVkNUnedlqYj7PAx0DiOccpYkfDiiXD-Ef9PsKWahpyP3EcApVkNPZemrrst7jOMsydb0ZbpxADquZJxZrQ4DpFquxY9lIG6XSUoAY9w1AAfPdUHLVQIy72h7bqLoQ)**
5. Configuración de nginx: Se instaló nginx en una nueva máquina de Ubuntu 18, luego definimos los puertos a los cuales se realiza el balanceo de cargas en el archivo default: **![](https://lh6.googleusercontent.com/WDScKFQEfWJ0NEYAY-QInmti9FEJ05R9KgoHiXQj4yT4AkHjQ-pExffT_gxsndX-IqbOH5tEUbj5oYlhFlgnKHiQVgMr8-1KenH265U8eraZkX-AJ_CWCP5CwuL9Wyl5Lg7EWFo)**
6. Drupal en nginx:**![](https://lh6.googleusercontent.com/RFhTQYo1zgbHachcMQQppYzaA75UKQs533apfLjZCu5wCHo5tQXpSs-YYk0sdXdVId8vLGw0X5yEax4zNVH2m8LmiKvYcyBh006o9MzqA5pRLWf6MeEsCAYtxpvLE4EnyIVoQ3Y)**
## Configuración del análisis de código estático
Para el análisis estático de código utilizamos la herramienta Codacy que nos brinda una manera simple y efectiva de realizar esta tarea.

1. Ir al sitio [https://www.codacy.com/](https://www.codacy.com/) y dar click en “Log in”**![](https://lh6.googleusercontent.com/4iGakGk8nIWlbDDffjijxexyyzB75yKmCJ1Bkt3QKAN0GLMpRsscSRk9y28e05bxiw5oU8NkqmBbVRD0JzGRmlcyZQgNfPzv3PVufQfsZf_a8S_JgOr65jnUF9XHQnfdaK3tsAQv)**
2. Seleccionamos GitLab ya que este es el servicio que estamos utilizando para almacenar nuestro código y realizar el proceso de DevOps.**![](https://lh5.googleusercontent.com/6Vytr74KXMZFrCgB7XLlH0Ow1kYafK6yzItyzI4-seUNv5ZNu6XVvc2NlcOc15cEhUi_0wmCsm4RP_IVYvFBzn5LvNcCxUoCJ-KshMzPrF3GLRH6EmUBlK7PNKoLCk-IwRHodxA9)**
3. Una vez entremos seleccionamos el repositorio que queremos analizar:**![](https://lh4.googleusercontent.com/JR8T2g6fc8i7oS89NjlzX0NFsayASZ0Lm6S5rS0OHXRNRWTDS41ptEmBtR-CvWDDFTFykWdqIyzmLtTtK1ZY04opjL3FZ1pgyAmQtSROeQCi2CQ29bLNqGOReVlA8-qiOLUaWs7p)**
4. Identificamos el repositorio que queremos usar y le damos en “Add”**![](https://lh3.googleusercontent.com/w2EPyHMlDtVOrEFvjTc4mYEx001gDzHzSAZYnt5gN953-rkjbPYX9BpHFPeTXRfWjDXDdQYVJmX_wIkHCWQo5NmLUDhp4GwFuDua-nvqiqa9ieY5qEnTV4s1wrUvlHQ9oE-HLaXi)**
5. Una vez lo añadamos vamos al dashboard principal de Codacy y ya podemos ver nuestro repositorio allí.**![](https://lh3.googleusercontent.com/p9jxJ_56-TiV-TUPDw9r8VvH-RIKHLpkgDtD709dzTIBBpBevRmZwjVKKMvmfVSEUli0fSWDzfRYAKwxlqUeVV6R1B3WWLgTKAua_MSvTZLkms5ug6WBGusZJ-53VgpUi8U6Y7LI)**
6. Ahora podemos seleccionar nuestro repositorio y ver el analisis que Codacy ha hecho sobre este.**![](https://lh6.googleusercontent.com/G7x62bKaJNzJ7lJH4V6bTUbgUN3WVxaA1w7Y8BWN15V_bcw_7lnC1rIYZklNmIa0HjALOYve4a0QCGTCy43RvBcJ4wbtkIQh7NwGicROjZ4O6tHZxeTrsO-V4Z0q5Jh3_Tmx_jb2)**
7. También podemos ver los commits que se han realizado y la calidad del codigo.**![](https://lh6.googleusercontent.com/Yx8AWiFM7d9-3LNK4xQoaRTWv7E8eXc085lq-Wbp6eFF7c65EeG44OWUloO98YI-BO6YsHgUkPNogBRtyQmhn6IQeNelqFcPerr_VyOpxjTes38JPwlozVWXMvuOideXFpmf0hE6)**
8. Podemos dar click en los issues que nos marca Codacy para obtener más detalles.**![](https://lh3.googleusercontent.com/r3RoJX8xyjelOAYwQT3dOzuqI_DIOByCKJhgO1sKTpBucU_zG-FM_B4HBQUwwyPwtDaKi9G9b_p5zzzNKYkIFSZoa8UWLFOMoDyNbcu4gnd3_8_iC1-yQzWs6YxTDQeQYOiLHLdN)**
9. Ahora que tenemos un mejor entendimiento del issue podemos solucionarlo.

## Configuración CDN
1. Loguearse en Drupal como administrador del sitio **![](https://lh4.googleusercontent.com/0GKBkYFmx78etW2M4g0dNyRslyMc8Y1KeXF4m0ae_bkZ4nSxPBhopbkJQGKkUH4JpLEANN_XYzB55BDUYwzdJZQZVxjdSm78bzd8yCXAZYM6lGTGZFqtf1lXMmUFcGJ5Pu9_FN1S)**
2. Descargar el modulo de CDN para Drupal: https://ftp.drupal.org/files/projects/cdn-8.x-3.4.zip 
3. **![](https://lh4.googleusercontent.com/rpYAWVKGexVcUizwqdFLaMDMz6C4Zh2tUKR8vldzQOEYp_rXyJcA0DEy1ZiMIn33Ogop1Iv_BSf9ShOuH1iH0y9tt5FTgh_rPzxG1MQxyiDhrWorUNux6jLKQegovXLEXjQnd38l)**
4. **![](https://lh4.googleusercontent.com/1esbdKCOvyF4nbNH2mEsPzfnWVfzRPPx0eNFZYx2utLiSh1SKlLzrdcv6tZvrSmv03F0TMzwr2hA6cBzdvxdPWuQOUsxVJzf15WKCb92TJImQTr0hnBA8IjtlhIzjOdBz2Rc_RSr)**
5. Vaya a la página Extender y, en Servicios web, seleccione "CDN" y "CDN UI" y luego haga clic en Instalar.**![](https://lh4.googleusercontent.com/rj7RzY5-FcBG0c2csOUFxX6J4aAmZMZUohID6rbLFVw7e15HKbJUn6q7J-gm1LFC3xHAJsYPrauid_mWcxjRaIsNrQBtV2rAHRG3nI0gODiByuqhFWg73AzEOmCtfBOXZ8cuzYSy)**
6. Registrarse en https://www.keycdn.com/
7. Crear una “Pull Zone” siguiendo las siguientes instrucciones: https://www.keycdn.com/support/create-a-pull-zone **![](https://lh6.googleusercontent.com/T_HxR9MzNpjlOKlrJlKlATdPDH9Mk-Y7EOTKynGAp2hB3xOA2y3R4PyM27xjW6hCQXK6vj9lVPMdM-JUQB0LRNwcu8ZS0AihhRHn2X2dVYf3z3B9y5vls2kMXYkboqRAhIxHWT4l)**
8. Vaya a Configuration> Web Services> CDN Integration. Establezca el estado de CDN en habilitado e ingrese la URL de CDN que obtiene del panel de KeyCDN.**![](https://lh4.googleusercontent.com/xrf6QjQed3CHM0vuZ81NJN0sblbbLbez3ZXsUTPXxY2WYMePYwySSl4OZ3RI2jQcT0Gc62dPjpI-8HbDTqJsyfvMWHQkVvb91xA42jSUuzk2e1-wo-CTCjzBfUvBBU_ISkHZF83Y)**
9. Dar click en “Save configuration”.

## Configuración de dominio público
1. Loguearse en https://my.freenom.com/clientarea.php?language=english o crear una cuenta
2. Ir al siguiente link y buscar un dominio gratis https://my.freenom.com/domains.php **![](https://lh4.googleusercontent.com/ZGuHylPI-ktyrongdD_b8ge3tytCUGByGJkzmdxAK94yupgUiR-xyop3aC-QSZH2WOHGFWseKCNt40POwE745rFe806wdCbWE16-ugl5ekromhvpJhF9YO6hrzsd52gRFZCyvIJ8)**
3. Seleccionar el dominio que queremos y realizar el proceso para “comprarlo” (como vale 0$ no necesitas ingresar ningún medio de pago)
4. **![](https://lh6.googleusercontent.com/cl28c6BtR7FK8MpTZdqZXaEsCDkVMizau0xjYq_F0tLhBS1NEXhZE2jKIGF4_WUMRtSnCzPWQsyqnfw0c0VKW-gg5NK3p51qHmHtVTKCAnrModjehpZemvWHJQ5ibjS1W5gomszt)**
5. Dar click en “Manage Domain” en el dominio que queremos usar para el sitio **![](https://lh4.googleusercontent.com/oA00OUvYbi8tAsHNPOEbT2ldfONWjSKBfcsyVok94riKRa-oimSZ6JG8z7CIQGaYsT52P1SHbepe8buUlNvYKzQdJdx8Fd2kExADxVg6Q8a8YA66wZv3yynh3ma5CFWSb_lLGKnl)**
6. **![](https://lh6.googleusercontent.com/fkDQZDoEltdPCYjvht2aDYRIBkcMgDQKdiqHmuG4jitSyaTfOcpPAahjH-WJw0mUGpzft0idxgXX-ZdsiAjQZWvsOFKe1DipLIVNvmqciQZuxlGHncDjtvc0Nh7MZHAJEERW9UlA)**
7. **![](https://lh3.googleusercontent.com/il4RnZTIcuqv7lVxQN0xCAy6uQ8wADFW-Ga0zao6RXbEEqviyHz4zoOGmklIvXRfQTDrHZwkUVOZV0zLJAC74bvK5sWAQLPHksetEarYi6gc_fFNzYcd54Lf9d77YY7H0D3V3zBH)**
8. Verificar que se puede acceder al sitio por medio del dominio que acabamos de configurar **![](https://lh3.googleusercontent.com/Xoi8fKxEL6r4Aoy7U2Nfk6yUQTWBaRsUyD0_KCyudWtERF2HhDMD-44v038O4l4Tljb69jb2cFvQp2JlgMJu4TLH83_h76gKCOJJ4iaa0a1m3qA3igJ_HOJU91bECmNWFCTP-wri)**
## Instalación y configuración de certificados SSL
1. Visitamos el sitio https://www.sslforfree.com/
2. Ponemos nuestro nombre de dominio y damos click en “Create Free SSL Certificate” **![](https://lh3.googleusercontent.com/KcMOuWq73syYQZRid0ii9wKrk3hlkIpxv1LmMoTXze9sOr_6XD70J0P-qHjjlYdy0Q7r9rg8sEgyeSEh8YcOG_pKzpY5vKhmymmNGt_PXcl0-sQKxsa_NwSolU7wgupNVrvXakmQ)**
3. Seleccionamos la opción “Manual Verification (DNS)” y damos click en el botón “Manually Verify Domain” **![](https://lh4.googleusercontent.com/Q_4VfGUFPo1USzwzmxfBJgR5dm8oRvY3mqLpxJZGqHiKGr0Vob4menWUezDHMd0B-lIyDBXkaGAO4WnOXZgZmeS1iRHzUdiorL2fEVPGwhWdL2FcXUD07W-xJT4Hk69f9XGKtR6j)**
4. La plataforma nos dará una serie de pasos que debemos seguir **![](https://lh6.googleusercontent.com/QmENxCD3yNMP5M2uQhUG3qExnRdtw4vxL2TnQxefjtYJnnbMPmqfGvUriffoJ98-drVjDDAXiCSubk3i1CzcF7BgD8x7UksSKmbSs3Pi2yyzq8AUmfLmjJKhwPan7xrUREACJn2u)**
5. Vamos al sitio de administración de nuestro dominio y añadimos los registros especificados **![](https://lh4.googleusercontent.com/IyNHndmfIkP5j99YUKe7iGDBYTS0uqLqRaS3wfboLuQBJxubpiz5V9aZJGZVbQjq5ibkDJ2vKtG0tpGQIEWzOfjPmen2Qh0qS-K2QORN_qFdz9c6nZx_kECjoWYGrbUwoKFTtDTn)**
6. Verificamos que los registros fueron añadidos correctamente visitando los links que referencian en la imagen del paso 4. **![](https://lh4.googleusercontent.com/2A6w43KlRE2qRHNZjPAMmnW9IWyXtBLSWTJDbUxdS9IdG-fjnQrsnkvhP7yxScmmksMaWVuEZRermKIB8opV4imSGPIdwcOC280qFOBtVBfMUuDvi2C2grMga8XAVM3_Lnvhzm0J)**
7. Después de esto damos click en el botón “Download SSL Certificates” y se descargará un zip con los certificados. 
8. Vamos a la instancia de amazon donde tengamos instalado nuestro balanceador de cargas y entramos por ssh **![](https://lh6.googleusercontent.com/nmXFBGbPZ71EoixFBlGt56EwSVOvA0jKmtDWv8MD41q1JQWtzPm8kc5dg6OiThQdMJH9i4cDHD4_LGVAdPdgn-HvB02NHlFKgEcU75o32PNb2A7stexZcIHOtFhvKIFc__BVkUVI)**
9. Vamos al directorio ```/etc/nginx/sites-available ``` 
10. Creamos un directorio y ponemos nuestros certificados que descargamos previamente en este directorio **![](https://lh4.googleusercontent.com/VCXWDCPg5f2bLbNkVOElqoae2rdbvGKXb3lDWm_P3KPtvMph5RuT7k5d3VyOajDrnLTuBmvRdmpf9gdDgLTWxoN03V-5ByUq-ie6IoIxNWwJimv0TjgudK7ctqMWs9s7LIAMkDQm)**
11. Abrimos la configuración del balanceador de cargas Nginx ponemos la siguiente configuración: **![](https://lh5.googleusercontent.com/2AqtkimnYOtZ7XsAQ5SfLHqwxx3IbyVHp50EfqBmKGa4tjF4XaLDGkCSqH7P0D_740sbopiJlMnbDYwez5-1cUsH--yLg6vIWbQH5jhOiXiJunpITp2TyaKcdY0FPRUFQDjTlPI_)**
12. Guardamos los cambios y reiniciamos el Nginx: **![](https://lh6.googleusercontent.com/LFwCNzV2k3PYkV1Q6ps0yIiHfa1t8w1qVQJVVS_FmQMon4mhzJIBqVo-gKJAS3XDtmPsnp9FZ1PdB1z1J2u4fZz2f69f7dFrw0Cy_5KFYGER5p9feqQ0p1LG2iLPd3Cj-wZ6C47B)**
13. Visitamos el sitio nuevamente, esta vez con https y verificamos que el sitio es seguro. **![](https://lh3.googleusercontent.com/MlO5dNimMKR9yU5JK6cQdQdPMRD_Gaqrs7Cwcig2FPlMmiUyoda1CfdJW_9l5bFg0n-KwYrrWVTTZXbhw5s25DqHPiClOu2t87QWzoiTNp3NV_EE5WfRKoxkE6z9ZHgIrUIoNMQd)**
## Configuración de login con cuenta Google
1. Loguearse en Drupal como administrador del sitio
2. **![](https://lh5.googleusercontent.com/fO238D1370nwzLkOF6vIGjsfeAH5mIBBTUGBRqS83AfokrphjncffYxiOBRmY-48VonkkfPwyD34v9xE9hSOyUZrf_2OWx4ddKtSIjp1YCsCKBtTh4rukhTuDbsBSRRNwV4biQRO)**
3. Usar el siguiente link para instalar el modulo de social login: https://ftp.drupal.org/files/projects/social_login-8.x-1.2.tar.gz  **![](https://lh5.googleusercontent.com/sJYV0lotC4r3FPAGLgFTKOvJbcvl2-LqLPOyHfE34HcUmQDQKl5QAzuOp1Njq-ybU4B5Sg9rJd2IqOq6KAQAyzHE3VqP-R47VuPJgYR3XBpMG_2nyHBF1YTdvXXIehqzTvyxYyDA)**
4. Ir de nuevo a “Extend” y buscar el módulo de social login, seleccionarlo y dar click en “Install” **![](https://lh5.googleusercontent.com/nKzzjcEs7W18_DGSQjLSsBT8T74J8Wo69xF1YQ18QKIznRsoLCFy6mR6lnB0OLFNuwLS4WBtExGgU7Xc1u8p_6DNqR7CS1T_AUlanZus8YYYUZ9nynxe3LgSQYGDugM3gCkSJVRW)**
5. Ir a la configuracion del modulo **![](https://lh6.googleusercontent.com/Pz6y0bRj07sKHMc0TCaAKhm9sAFr0L8X58v5UFVrxTlKbJNTgxbVob8sMmkwnJsYZahtnLeJidV5DRaEbvtHKbWgdPbbK6SP6P1y14Q0gLW3GFP3QGC4BPIySVeIkA0OHkj_SsBC)**
6. **![](https://lh5.googleusercontent.com/OUarbcKzRf-czqNwILsGw4r2qRJRQzpC_GJquYssvv93tzJHf4_7N_y_5AxkBKKtwGIYkohpRbD4ZP72MBLzhdE-JbXlohre1uAVEe3ch6YjQNazWaoMGj7f4jhbOD3yJqkAsHLY)**
7. Ir a https://app.oneall.com/applications/, registrarse y crear un nuevo proyecto. **![](https://lh3.googleusercontent.com/vc8j1xja9Q5ibv73fik34Eqjhv1hU0oPe4JXyOTaosUYxzRKD4z8J-JZe7uXZ4D_x4am48cddBdO5WV0llpS1h2QcM3Ow3a_dIt8_kRbbdBFuEDjttzelzmRH8MDrHNT18F-rBVA)** 
8. Ingresar la dirección del proyecto al que queremos integrar con el login de Google. **![](https://lh3.googleusercontent.com/Nzm6BvyTYooJ06G4XBjes1NvyActAnAlIYnoAibYtVKgwt2C1muisT9HZrvd5sURsE5La4CIn42BPNcVRmDI2R9J67jKwUy1dysZwvyaFrZu_5FfStzo577QdXW0vMINd9Vaqm-J)** 
9. Sigue las instrucciones que se te muestran. **![](https://lh4.googleusercontent.com/snrmAulOoaXYLwygnfVRXlR0Dfj-7W0DB5lLxzfPgyNsyD6k9EV-wa_A2TwU2EiHpdGaPRYWPbLYryfAFAyyuOZL5r5CT4zIsdVd0G3Rjr9FcleUb4BAGlBGE5RQ0gCmmxUyxhid)**
10. Cuando termines los pasos que se te dieron te darán unos códigos que debes ingresar en las configuraciones de Drupal **![](https://lh6.googleusercontent.com/cADl_7tg-Cfd4KYtHdcp6I9CrZs3vQcVbleVNIN1xA5ty-Al2RjLCy2gQj9kvj2Jp76JmeEH7IZjiQbudZIFlg-Czt6pmkdTGTLmht1ZgddQkB7RHB0Cw-3uojnCujkgedr1lneD)**
11. Guarda los cambios y ve la pagina de login de drupal, debe aparecer el icono para loguearse con una cuenta de Google. **![](https://lh6.googleusercontent.com/LO0CwlhOtFMc5Xe03bhX85gWhWOgk8Cybiz6wsIJKmDOdUgs76jMzuGLph6wp_-wW2LR0MYzK_26XRavWA4VJmzlw_r89FAuTTe9ccX7mYlNEzOvHNrCmEqPB6EP_HLl44zVay6U)**
## Configuración Amazon RDS
Para la creación de la base de datos se usó el servicio que provee Amazon RDS (Relational Database Service). Gracias a este servicio no es necesario preocuparnos por la creación de copias de seguridad, porque esto hace parte de las ventajas de usar el RDS de Amazon.

Para crear una base de datos a través del servicio de RDS, realizamos los siguientes pasos:
1. Buscamos el servicio RDS en el catálogo de servicios de Amazon y lo seleccionamos. **![](https://lh6.googleusercontent.com/rZCtTZYNA_Hwv0j9jgFYnk76mRRkSAkwcdKqYJO7mNsw6MmAhj-YpDgOyyUWuHGw-mexLd21fN3aePt9S_oZlM7AZloK5B4wccR5TEN0MiLYonzBHQXcPzt6wuEzV48ZtO97KQAT)**
2. Seleccionamos la creación standart y postgres como nuestro sistema de gestión de base datos: **![](https://lh6.googleusercontent.com/V0X0VpYyJjuV1loYujEwkJfRv6O6jV4lCf_BH_Xvy4JRbZRHA8_u3yxE9g6B-zH5QPLQozaztH0Lx2HtJDhfSCxtewytcriEnEfzpPpYOMCZ0qsCdoMCbT-xVJQGVWODGSuJBkxC)**
3. Aquí es necesario tomar nota del usuario administrador de la base de datos y la contraseña además del nombre que le vamos a dar a la base de datos. **![](https://lh6.googleusercontent.com/kUYoYJfR2cImSVubD_QImB9UeSqFZ54ezsR9V2GPzvEdh8QE5Rg6Gj1g2utjryf2zwbKX96xIAX7S6n-3ZY3wKohJs-xyMZKvCbXBdaPiIFG1BBBcOTV6zQPsdJrfA34uch5j70E)**
4. Si nos dirigimos a la configuración avanzada podemos ver que el servicio de backup se encuentra activo y se realizará cada 7 días. **![](https://lh3.googleusercontent.com/Xy_7yq7-7zZYr3NN_US6tJrRmtnYxAOGXcuvKZsGPm6YO3eS2zYebUsPntZSj9h0DIEvFfFUSOvqM8R0nzoYJ-QpVrOGdF_cz2ASS-rmx8LDYCY8XJijVpBRaAnif16Cnf4qHUTo)**